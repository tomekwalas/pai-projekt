<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents('php://input');
		$_POST = json_decode($post,true);

		
	}
	public function get($id=false)
	{
		$this->load->model('site/Products_model');
		$result = $this->Products_model->get($id);
		echo json_encode($result);
	}
	public function getImages($id)
	{
		$basePath = FCPATH .'..'. DIRECTORY_SEPARATOR . 'img'. DIRECTORY_SEPARATOR. 'oferta'.DIRECTORY_SEPARATOR;
		$basePath = $basePath . $id . DIRECTORY_SEPARATOR;

		if(!is_dir($basePath))
			return;
		$files = scandir($basePath);
		$files = array_diff($files, array('.','..'));

		$newFiles = array();
		foreach ($files as $file)
		{
			$newFiles[] .=$file;
		}
		echo json_encode($newFiles);
	}
	public function getGalery()
	{
		$basePath = FCPATH .'..'. DIRECTORY_SEPARATOR . 'img'. DIRECTORY_SEPARATOR. 'galeria'.DIRECTORY_SEPARATOR;

		if(!is_dir($basePath))
			return;
		$files = scandir($basePath);
		$files = array_diff($files, array('.','..'));

		$newFiles = array();
		foreach ($files as $file)
		{
			$newFiles[] .=$file;
		}
		echo json_encode($newFiles);
	}

}