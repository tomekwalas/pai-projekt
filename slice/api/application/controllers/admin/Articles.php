<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents('php://input');
		$_POST = json_decode($post,true);
		//print_r($_POST);
		$this->load->model('admin/Articles_model');
	}
	
	public function create()
	{
		$article = $this->input->post('inputs');
		$this->Articles_model->create($article);

	}
	
}