'use strict';

var controllersNavigation = angular.module( 'controllersNavigation' , [] );


controllersNavigation.controller('navigation', ['$scope', '$location','checkToken', function($scope,$location,checkToken){
	
		//console.log(checkToken.raw());
		if ( checkToken.loggedIn() )
				$scope.loggedIn = true;
			else
				$scope.loggedIn = false;
		
		if ( checkToken.isAdmin() )
				$scope.isAdmin = true;
			else
				$scope.isAdmin = false;
			
		$scope.navigation = function(){
			if ( checkToken.loggedIn() )
			return 'partials/site/navigation.html' ;
		else
			return false;
			}
			
		

		$scope.isActive = function( path ) {
			return $location.path() == path;

		};

		

		$scope.logout = function(){
			checkToken.del();
			location.reload();
		}
		
				

}]);

