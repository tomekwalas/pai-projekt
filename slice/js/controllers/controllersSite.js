'use strict';

var controllersSite = angular.module( 'controllersSite' , [] );

/*controllersSite.controller('login', ['$scope','$rootScope', '$http','$location', function($scope,$rootScope,$http,$location){
	
//TODO pobrac dane z formularza i sprawdzic poprawjnosc
document.title = 'Blogos - logowanie';
	//if ( checkToken.loggedIn() )
	//	$location.path( '/admin/shop' );

	$scope.input = {};
	$scope.errors = {};
	$scope.user={};
	$scope.lol="a";
	$scope.formSubmit = function( user ){
		console.log(user);
		/*$http.post('login.php',{
			email:user.email,
			password:user.password
		}).success( function(data){
			


			$scope.submit=true;
			$scope.erroe = data.error;
			
			//if(!data.error)
			//{
				//store.set('token' , data.token);
		//		location.reload();
		//	}
			
			
		}).error( function(){
			console.log('blad komunikacji z API');
		});

		var req = {
				method: 'POST',
				url: './login.php',
				headers: {
				  'Content-Type': "application/x-www-form-urlencoded"
				},
				data: "data=" + JSON.stringify({
					email: user.email,
					password: user.password
				})
			}
			$http(req).then(function(lol) {
		    	console.log(lol.data);
				$scope.lol=lol.data;

		    	//console.log($scope.oldValue);
			});
			if($scope.lol=="true"){
				$rootScope.name = 'zalogowany';
				$location.path( '/podglad' );}
	};	
	
}]);*/

controllersSite.controller('login', ['$scope', '$http','store','$location','checkToken', function($scope,$http,store,$location,checkToken){
	
//TODO pobrac dane z formularza i sprawdzic poprawjnosc
document.title = 'Blogos - logowanie'
	if ( checkToken.loggedIn() )
		$location.path( '/podglad' );

	$scope.input = {};
	//$scope.errors = {};
	$scope.user={};
	$scope.formSubmit = function( user ){
		$http.post('api/site/user/login/',{
			email:user.email,
			password:user.password
		}).success( function(data){
			


			$scope.submit=true;
			$scope.error = data.error;
			
			if(!data.error)
			{
				store.set('token' , data.token);
				location.reload();
			}
			
			
		}).error( function(){
			console.log('blad komunikacji z API');
		});

	};
	//var token = store.get('token');
	console.log( checkToken.payload());
	

}]);

controllersSite.controller('podglad', ['$scope','$rootScope','$location', '$http', '$routeParams','$window','checkToken', function($scope,$rootScope,$location,$http,$routeParams,$window,checkToken){
	
		if ( !checkToken.loggedIn() )
		$location.path( '/login' );
		document.title = 'Blogos - podglad'; 
		$http.get('abc.php').
		success( function(data){
			$scope.products = data;
			//console.log(data);
		}).error( function(data){
			console.log('blad polaczenia z API')
		});		

}]);

controllersSite.controller('podgladEdit', ['$scope','$rootScope','$location', '$http', '$routeParams','$window','checkToken', function($scope,$rootScope,$location,$http,$routeParams,$window,checkToken){
		

		if ( checkToken.isAdmin() )
		$location.path( '/admin/podglad' );
	else
		return false;
		document.title = 'Admin Blogos - podglad'; 
		$http.get('abc.php').
		success( function(data){
			$scope.products = data;
			//console.log(data);
		}).error( function(data){
			console.log('blad polaczenia z API')
		});
		$scope.count=0;
		
		$scope.myFunc = function($event,oldValue) {
        console.log($scope.count);
        $scope.oldValue=oldValue;
        $scope.count++;
        //$window.alert("zmiana linkow w budowie");
		        var req = {
				method: 'POST',
				url: './podmiana.php',
				headers: {
				  'Content-Type': "application/x-www-form-urlencoded"
				},
				data: "data=" + JSON.stringify({
					wpis: $event.target.value,
					old: $scope.oldValue
				})
			}
			$http(req).then(function() {
		    	console.log($event.target.value);
		    	console.log($scope.oldValue);
		    	$http.get('abc.php').
		success( function(data){
			$scope.products = data;
			//console.log(data);
		}).error( function(data){
			console.log('blad polaczenia z API')
		});
			});
      };


}]);

controllersSite.controller('register', ['$scope', '$http', function($scope,$http){
	
	//TODO pobrac dane z formularza i sprawdzic poprawjnosc
	document.title = 'Blogos - rejestracja'

	$scope.input = {};
	$scope.errors = {};
	$scope.user={};
	$scope.formSubmit = function( user ){

		$http.post('api/site/user/create/',{
			user:user,
			name:user.name,
			email:user.email,
			password:user.password,
			passconf:user.passconf
		}).success( function(errors){
			$scope.submit=true;
			$scope.user={};
			
			if(errors)
			{
				$scope.errors = errors;
			}
			else
			{
				$scope.errors = {};
				$scope.success=true;
			
			}
			
		}).error( function(){
			console.log('blad komunikacji z API');
		});
		

	};

}]);

controllersSite.controller( 'users' , [ '$scope' , '$http' , 'checkToken' , function( $scope , $http , checkToken ){

	$http.post( 'api/admin/users/get' , {
		token: checkToken.raw()
	}).success( function( data ){
		$scope.users = data;
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

	$scope.delete = function ( user , $index ) {

		if ( !confirm( 'Czy na pewno chcesz usunąć tego użytkownika?' ) )
			return false;

		$scope.users.splice( $index , 1 );

		$http.post( 'api/admin/users/delete/' , {
			token: checkToken.raw(),
			user : user
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};


}]);

controllersSite.controller( 'userEdit' , [ '$scope' , '$http' , '$routeParams' , '$timeout' , 'checkToken' , function( $scope , $http , $routeParams , $timeout , checkToken ){

	var userId = $routeParams.id;
	$scope.id = userId;

	$http.post( 'api/admin/users/get/' + userId , {
		token: checkToken.raw()
	}).success( function( data ){
		$scope.user = data;
		console.log( data );
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

	$scope.saveChanges = function ( user ) {

		$http.post( 'api/admin/users/update/' , {
			token: checkToken.raw(),
			user : user,
			id : userId,
			name : user.name,
			email : user.email,
			password : user.password,
			passconf : user.passconf
		}).success( function( errors ){

			$scope.submit = true;
			
			if ( errors )
			{
				$scope.errors = errors;
			}
			else
			{
				$scope.success = true;
				$timeout(function(){
					$scope.success = false;
					$scope.product = {};
				} , 3000 );
			}

		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};


}]);

controllersSite.controller('blog', ['$scope','$location', '$http', '$routeParams', '$compile', function($scope,$location,$http,$routeParams,$compile){
	

		document.title = 'Artykuły - Blogos';
		$http.get('api/site/articles/get').
		success( function(data){
			$scope.articles = $compile(data)($scope);
			console.log($scope.articles);
		}).error( function(data){
			console.log('blad polaczenia z API')
		});

}]);

controllersSite.controller('addArticles', ['$scope', '$http', '$timeout', function($scope,$http,$timeout){
	
		document.title = 'Dodaj wpis - Blogos';
		$scope.inputs = {
	  tresc: 1
  };
  $scope.items = [{name:'tresc'}];

		$scope.dodaj = function(){
		 	console.log("dodaj");
		 	var itemNumber = $scope.items.length + 1;
		var inputName = 'tresc' + itemNumber;
		$scope.items.push({name: inputName});
		$scope.inputs[inputName] = itemNumber;
		 };
		
		$scope.usun = function(){
			$scope.items.splice(-1,1)
		}
		$scope.createArticle = function( inputs ) {


		 	

			$http.post('api/admin/articles/create/',{
				inputs:inputs
			}).success( function(){
				$scope.success=true;
				$timeout(function(){
					$scope.success=false;
					$scope.inputs={};
				} , 3000)
			}).error( function(){
				console.log('blad komunikacji z API');
				console.log(inputs);
			});
		};
}]);
