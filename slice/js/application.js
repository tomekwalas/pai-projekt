'use strict';

var app = angular.module( 'app' , ['ngRoute', 'angular-storage' , 'angular-jwt' ,'controllersSite','controllersNavigation', 'myServices'] );

app.config( [ '$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){
	
	// ============== Site =====================

	$routeProvider.when( '/podglad' , {
		controller : 'podglad' ,
		templateUrl : 'partials/site/podglad.html'
	});

	
	// ============== AdminP =====================

	$routeProvider.when( '/admin/podglad' , {
		controller : 'podgladEdit' ,
		templateUrl : 'partials/admin/podgladEdit.html'
	});

	$routeProvider.when( '/admin/uzytkownicy' , {
		controller: 'users',
		templateUrl : 'partials/admin/users.html'
	});

	$routeProvider.when( '/admin/user/edit/:id' , {
		controller: 'userEdit',
		templateUrl : 'partials/admin/user-edit.html'
	});

	$routeProvider.when( '/admin/addBlog' , {
		controller: 'addArticles',
		templateUrl : 'partials/admin/addBlog.html'
	});
	// ============== Login =====================

	$routeProvider.when( '/login' , {
		controller : 'login' ,
		templateUrl : 'partials/site/login.html'
	});

	$routeProvider.when( '/blog' , {
		controller : 'blog' ,
		templateUrl : 'partials/site/blog.html'
	});

	// ============== Rejestracja =====================

	$routeProvider.when( '/register' , {
		controller : 'register' ,
		templateUrl : 'partials/site/register.html'
	});


	
	// ============== Default =====================

	$routeProvider.otherwise({
		redirectTo: '/login'
	});
}]);

