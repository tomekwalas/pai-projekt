<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
   
    
    <title>Blogos</title>
    <meta name="description" content="Usuń wpisy jak ktoiś wypowiedział umowe :D" />

    <!--Login-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>

  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">

  <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
  
  <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">

  <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">

  <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
  
  <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">

  <link rel="stylesheet" type="text/css" href="css/util.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--Koniec-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script  src="js/jquery-1.11.0.min.js"></script>

	<script src="js/angular.min.js"></script>
  <script src="js/angular-route.min.js"></script>
  <script src="js/angular-storage.js"></script>
  <script src="js/angular-jwt.js"></script>
  

  
  <!-- <script src="https://cdn.rawgit.com/nervgh/angular-file-upload/master/angular-file-upload.min.js"></script> -->
    <!-- Bootstrap -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="Stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body ng-app="app">
<div ng-controller="navigation" ng-include="navigation()">
</div>
	<div ng-view>
	</div>
	
	

<!-- ______________________js_______________ -->
  <script src="js/application.js"></script>
  <script src="js/services.js"></script>
  <script src="js/directives.js"></script>

 
  <script src="js/controllers/controllersSite.js"></script>
   <script src="js/controllers/controllersNavigation.js"></script>

  <!-- Reszta -->
  <script src="vendor/jquery/jquery-3.2.1.min.js"></script>

  <script src="vendor/animsition/js/animsition.min.js"></script>

  <script src="vendor/bootstrap/js/popper.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  <script src="vendor/select2/select2.min.js"></script>

  <script src="vendor/daterangepicker/moment.min.js"></script>
  <script src="vendor/daterangepicker/daterangepicker.js"></script>

  <script src="vendor/countdowntime/countdowntime.js"></script>

  <script src="js/main.js"></script>
  </body>
</html>